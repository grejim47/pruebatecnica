﻿using ApiProperties.Models;
using ApiProperties.Models.Context;

namespace ApiProperties.Interfaces
{
    public interface IProperty
    {
        Task<bool> CreateProperty(NewProperty NewProperty);
        Task<bool> AddImage(Imagen imagen);
        Task<bool> ChangePrice(ChancePrice chancePrice);
        Task<List<PropertyImage>> GetPropertieImages(int idProperty);
        Task<Property> GetPropertieDetails(int idProperty);
        Task<List<Property>> GetProperties();
    }
}
