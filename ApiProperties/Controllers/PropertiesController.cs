﻿using ApiProperties.Interfaces;
using ApiProperties.Models;
using ApiProperties.Models.Context;
using Microsoft.AspNetCore.Mvc;

namespace ApiProperties.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PropertiesController : ControllerBase
    {
        private readonly IProperty _iproperty;

        public PropertiesController(IProperty property)
        {
            _iproperty = property;
        }

        [HttpGet]
        [Route("GetProperties")]
        public async Task<List<Property>> GetProperties()
        {
            return await _iproperty.GetProperties();
        }

        [HttpGet]
        [Route("GetPropertieDetails")]
        public async Task<Property> GetPropertieDetails(int idProperty)
        {
            return await _iproperty.GetPropertieDetails(idProperty);
        }

        [HttpGet]
        [Route("GetPropertieImages")]
        public async Task<List<PropertyImage>> GetPropertieImages(int idProperty)
        {
            return await _iproperty.GetPropertieImages(idProperty);
        }

        [HttpPost]
        [Route("CreateProperty")]
        public async Task<bool> CreateProperty(NewProperty newProperty)
        {
            return await _iproperty.CreateProperty(newProperty);
        }

        [HttpPost]
        [Route("AddImage")]
        public async Task<bool> AddImage(Imagen newImagen)
        {
            return await _iproperty.AddImage(newImagen);
        }

        [HttpPut]
        [Route("ChangePrice")]
        public async Task<bool> ChangePrice(ChancePrice chancePrice)
        {
            return await _iproperty.ChangePrice(chancePrice);
        }
    }
}
