﻿namespace ApiProperties.Models.Context
{
    public class Owner
    {
        public int IdOwner { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Photo { get; set; }
        public DateTime Birthday { get; set; }

        public Owner(NewOwner newOwner)
        {
            this.Name = newOwner.Name;
            this.Address = newOwner.Address;
            this.Photo = newOwner.Photo;
            this.Birthday = newOwner.BirthDay;
        }
    }
}
