﻿namespace ApiProperties.Models.Context
{
    public class Property
    {
        public int IdProperty { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public double Price { get; set; }
        public int CodeInternal { get; set; }
        public int Year { get; set; }
        public int IdOwner { get; set; }
        public virtual Owner Owner { get; set; }
        public Property(NewProperty newProperty, int idOwner) 
        {
            this.Name = newProperty.Name;
            this.Address = newProperty.Address;
            this.Price = newProperty.Price;
            this.CodeInternal = newProperty.CodeInternal;
            this.Year = newProperty.Year;
            this.IdProperty = idOwner;
        }
    }
}
