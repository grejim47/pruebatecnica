﻿namespace ApiProperties.Models.Context
{
    public class PropertyImage
    {
        public int IdPropertyImage { get; set; }
        public int IdProperty { get; set; }
        public string File { get; set; }
        public bool Enable { get; set; }

        public PropertyImage(Imagen imagen)
        {
            this.IdProperty = imagen.IdProperty;
            this.File = imagen.File;
            this.Enable = true;
        }
    }
}
