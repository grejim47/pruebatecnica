﻿namespace ApiProperties.Models.Context
{
    public class PropertyTrace
    {
        public int IdpropertyTrace { get; set; }
        public DateTime DateSale { get; set; }
        public string Name { get; set; }
        public double Value { get; set; }
        public double Tax { get; set; } 
        public int IdProperty { get; set; }
    }
}
