﻿namespace ApiProperties.Models
{
    public class ChancePrice
    {
        public double Price { get; set; }
        public int IdProperty { get; set; }
    }
}
