﻿namespace ApiProperties.Models
{
    public class Imagen
    {
        public int IdProperty { get; set; }
        public string File { get; set; }
    }
}
